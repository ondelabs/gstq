package com.ankur.gstq.solution.model;

import org.junit.Test;

import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class SentenceTest {

    private static final String GOD = "GOD";
    private static final String SAVE = "SAVE";
    private static final String THE = "THE";
    private static final String QUEEN = "QUEEN";
    private static final String EXPECTED_FIRST_SOLUTION = "GSTQ";
    private static final String EXPECTED_LAST_SOLUTION = "DEEN";
    private static final String EXPECTED_ONE_WORD_FIRST_SOLUTION = "Q";
    private static final String EXPECTED_ONE_WORD_LAST_SOLUTION = "N";

    private static final String EXPECTED_FIRST_FOR_SIMPLE_TEST_SOLUTION = "ad";
    private static final String EXPECTED_LAST_FOR_SIMPLE_TEST_SOLUTION = "bf";
    private static final List<String> EXPECTED_ALL_FOR_SIMPLE_TEST = Arrays.asList("ad", "ae", "af", "bd", "be", "bf");

    @Test
    public void ensureOutOfBoundsIsNotPossible() {
        // given
        Sentence sentence = givenSentence();

        // when
        String solutionMinusOne = sentence.get(-1);
        String solutionMax = sentence.get(Integer.MAX_VALUE);

        // then
        assertEquals(EXPECTED_FIRST_SOLUTION, solutionMinusOne);
        assertEquals(EXPECTED_LAST_SOLUTION, solutionMax);
    }

    @Test
    public void getFirstSolution() {
        // given
        Sentence sentence = givenSentence();

        // when
        String solution = sentence.get(0);

        // then
        assertEquals(EXPECTED_FIRST_SOLUTION, solution);
    }

    @Test
    public void getLastSolution() {
        // given
        Sentence sentence = givenSentence();

        // when
        String solution = sentence.get(179);

        // then
        assertEquals(EXPECTED_LAST_SOLUTION, solution);
    }

    @Test
    public void oneWordSentenceFirstSolution() {
        // given
        Sentence sentence = givenOneWordSentence();

        // when
        String solution = sentence.get(0);

        // then
        assertEquals(EXPECTED_ONE_WORD_FIRST_SOLUTION, solution);
    }

    @Test
    public void lastWordSentenceFirstSolution() {
        // given
        Sentence sentence = givenOneWordSentence();

        // when
        String solution = sentence.get(4);

        // then
        assertEquals(EXPECTED_ONE_WORD_LAST_SOLUTION, solution);
    }

    @Test
    public void ensureCorrectFirstAndLastSolutionForSimpleTest() {
        // given
        Sentence sentence = givenSimpleExampleSentence();

        // when
        String firstSolution = sentence.get(0);
        String lastSolution = sentence.get(5);

        // then
        assertEquals(EXPECTED_FIRST_FOR_SIMPLE_TEST_SOLUTION, firstSolution);
        assertEquals(EXPECTED_LAST_FOR_SIMPLE_TEST_SOLUTION, lastSolution);
    }

    @Test
    public void getAllSolutionsForSimpleTest() {
        // given
        Sentence sentence = givenSimpleExampleSentence();

        // when
        List<String> allSolutions = sentence.allSolutions();

        // then
        assertEquals(EXPECTED_ALL_FOR_SIMPLE_TEST, allSolutions);
    }

    private Sentence givenSentence() {
        Sentence sentence = new Sentence();
        sentence.add(QUEEN);
        sentence.add(THE);
        sentence.add(SAVE);
        sentence.add(GOD);
        return sentence;
    }

    private Sentence givenOneWordSentence() {
        Sentence sentence = new Sentence();
        sentence.add(QUEEN);
        return sentence;
    }

    private Sentence givenSimpleExampleSentence() {
        Sentence sentence = Sentence.from("ab def");
        return sentence;
    }
}