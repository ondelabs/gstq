package com.ankur.gstq.solution.resource;

import com.ankur.gstq.GstqApplication;
import com.ankur.gstq.solution.dto.SentenceDto;
import com.jayway.restassured.RestAssured;
import com.jayway.restassured.http.ContentType;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import static com.jayway.restassured.RestAssured.given;
import static org.junit.Assert.assertEquals;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = GstqApplication.class)
@WebAppConfiguration
@IntegrationTest("server.port:0")
public class SolutionResourceTest {

    @Value("${local.server.port}")
    int port;

    @Before
    public void setup() {
        RestAssured.port = this.port;
    }

    @Test
    public void makeSureJsonReturnedWithCorrectSolution() {
        SentenceDto sentenceDto = new SentenceDto();
        sentenceDto.setSentence("ab def");

        String responseBody = given().
                body(sentenceDto).
                contentType(ContentType.JSON).
                accept(ContentType.JSON).
        when().
                post("/solve").
        then().
                statusCode(200).
                contentType(ContentType.JSON).
                extract().
                response().
                body().
                print();

        assertEquals("[\"ad\",\"ae\",\"af\",\"bd\",\"be\",\"bf\"]", responseBody);
    }

    @Test
    public void ensure400IsReturnedWhenEmptyUrlIsSent() {
        given().
                body(new SentenceDto()).
                contentType(ContentType.JSON).
                accept(ContentType.JSON).
        when().
                post("/solve").
        then().
                statusCode(400);
    }
}