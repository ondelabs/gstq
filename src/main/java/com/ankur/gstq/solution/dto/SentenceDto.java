package com.ankur.gstq.solution.dto;

public class SentenceDto {
    private String sentence;

    public String getSentence() {
        return sentence;
    }

    public void setSentence(String sentence) {
        this.sentence = sentence;
    }
}
