package com.ankur.gstq.solution.resource;

import com.ankur.gstq.solution.dto.SentenceDto;
import com.ankur.gstq.solution.model.Sentence;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.logging.Logger;

@RestController
class SolutionResource {

    private static final Logger logger = Logger.getLogger("SolutionResource");

    @RequestMapping(value = "/solve", method = RequestMethod.POST,
            consumes = "application/json", produces = "application/json")
    public List<String> create(@NotNull @RequestBody final SentenceDto sentenceDto) {
        logger.info("Request body is " + sentenceDto.getSentence());
        validate(sentenceDto);
        Sentence sentence = Sentence.from(sentenceDto.getSentence());
        List<String> solutions = sentence.allSolutions();
        return solutions;
    }

    private void validate(final SentenceDto sentenceDto) {
        if (sentenceDto.getSentence() == null || sentenceDto.getSentence().length() == 0) {
            throw new IllegalArgumentException();
        }
    }
}
