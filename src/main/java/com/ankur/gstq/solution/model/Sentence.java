package com.ankur.gstq.solution.model;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.IntStream;

import static java.util.stream.Collectors.toList;

public class Sentence {
    private final List<Word> words = new ArrayList<>();
    private int totalVariations = 1;

    public static Sentence from(final String sentence) {
        Sentence result = new Sentence();
        String[] words = sentence.split("\\s+");
        revRange(0, words.length)
                .forEach(i -> result.add(words[i]));
        return result;
    }

    static IntStream revRange(int from, int to) {
        return IntStream.range(from, to).map(i -> to - i + from - 1);
    }

    public void add(final String word) {
        totalVariations *= word.length();
        words.add(Word.from(word, calculateVariationsUntilSwap()));
    }

    public List<String> allSolutions() {
        return IntStream.range(0, totalVariations)
                .mapToObj(i -> get(i))
                .collect(toList());
    }

    public String get(final int solutionNumber) {
        int cappedSolutionNumber = capSolutionNumber(solutionNumber);
        StringBuilder result = new StringBuilder();
        words.stream()
                .map(w -> w.get(cappedSolutionNumber))
                .forEach(c -> result.append(c));
        return result.reverse().toString();
    }

    public int capSolutionNumber(final int solutionNumber) {
        return Math.min(Math.max(0, solutionNumber), totalVariations-1);
    }

    private int calculateVariationsUntilSwap() {
        int wordVariations = 1;
        for (Word word : words) {
            wordVariations *= word.getNumberCharacters();
        }
        return wordVariations;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Sentence sentence = (Sentence) o;

        return words != null ? words.equals(sentence.words) : sentence.words == null;

    }

    @Override
    public int hashCode() {
        return words != null ? words.hashCode() : 0;
    }

    @Override
    public String toString() {
        return "Sentence{" +
                "words=" + words +
                ", totalVariations=" + totalVariations +
                '}';
    }
}
