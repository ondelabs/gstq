package com.ankur.gstq.solution.model;

class Word {
    private final String value;
    private final int variationsUntilSwap;

    private Word(final String value, final int variationsUntilSwap) {
        this.value = value;
        this.variationsUntilSwap = variationsUntilSwap;
    }

    public static Word from(final String value, final int variationsUntilSwap) {
        return new Word(value, variationsUntilSwap);
    }

    public char get(final int solutionNumber) {
        int index = (solutionNumber / variationsUntilSwap) % value.length();
        return value.charAt(index);
    }

    public int getNumberCharacters() {
        return value.length();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Word word = (Word) o;

        if (variationsUntilSwap != word.variationsUntilSwap) return false;
        return value != null ? value.equals(word.value) : word.value == null;

    }

    @Override
    public int hashCode() {
        int result = value != null ? value.hashCode() : 0;
        result = 31 * result + variationsUntilSwap;
        return result;
    }

    @Override
    public String toString() {
        return "Word{" +
                "value='" + value + '\'' +
                ", variationsUntilSwap=" + variationsUntilSwap +
                '}';
    }
}
