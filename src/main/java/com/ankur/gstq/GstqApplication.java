package com.ankur.gstq;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GstqApplication {

	public static void main(String[] args) {
		SpringApplication.run(GstqApplication.class, args);
	}
}
