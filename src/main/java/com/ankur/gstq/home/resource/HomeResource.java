package com.ankur.gstq.home.resource;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
class HomeResource {

    @RequestMapping("/")
    public String getHomePage() {
        return "home";
    }
}
