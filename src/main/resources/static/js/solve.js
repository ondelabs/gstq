function solveSentence() {
    var sentenceVar = $('#sentence').val();
    var requestBody = {sentence: sentenceVar};
    $.ajax({
        type: 'POST',
        url: '/solve',
        data: JSON.stringify(requestBody),
        success: function(data) {
            $('#response').val(data)
        },
        error: function(xhr,status,error) {
            $('#response').val("Please enter a sentence")
        },
        contentType: "application/json",
        dataType: 'json'
    });
}